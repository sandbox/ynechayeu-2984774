Entity hash watcher module.
-------------------

DESCRIPTION
-----------

Adds new field to selected entity types containing hash value based on values of selected fields.
Used to track changes on some selected set of fields. Fires an event when entity hash changes.

CONFIGURATION
-------------
The configuration page is at admin/config/entity-hash-watcher,
where you can select a list of fields to track per each entity type.
