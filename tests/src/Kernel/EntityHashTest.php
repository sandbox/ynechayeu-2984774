<?php

namespace Drupal\Tests\entity_hash_watcher\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\entity_hash_watcher\EntityHashChangedEvent;

/**
 * Tests the entity hash watcher module.
 *
 * @group entity_hash_watcher
 */
class EntityHashTest extends EntityKernelTestBase {

  /**
   * State service for recording information received by event listeners.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['node', 'entity_hash_watcher'];

  /**
   * Configuration mapping.
   *
   * @var array
   */
  protected $hashedFields = [
    'node' => [
      'test' => [
        'title',
      ]
    ],
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->state = \Drupal::state();

    // Create the node bundles required for testing.
    $type = $this->entityManager
      ->getStorage('node_type')
      ->create([
        'type' => 'test',
        'name' => 'test',
      ]);
    $type->save();

    $this->installSchema('node', 'node_access');

    $this->config('entity_hash_watcher.config')
        ->set('fields', $this->hashedFields)
        ->save();

    $this->container->get('entity.definition_update_manager')->applyUpdates();

    \Drupal::service('event_dispatcher')->addListener(EntityHashChangedEvent::HASH_CHANGED,
      [$this, 'entityHashEventRecorder']);
  }

  /**
   * Tests that entity hash value do not change.
   */
  function testHashNotChanges() {
    $entity = $this->entityManager
      ->getStorage('node')
      ->create([
        'type' => 'test',
        'title' => $this->randomString(),
      ]);
    $entity->save();

    $hash = $entity->get('entity_hash')->value;

    $entity->save();

    $new_hash = $entity->get('entity_hash')->value;

    $this->assertEqual($hash, $new_hash);
  }

  /**
   * Tests that entity hash value changes.
   */
  function testHashChanges() {
    $entity = $this->entityManager
      ->getStorage('node')
      ->create([
        'type' => 'test',
        'title' => $this
          ->randomString(),
      ]);
    $entity->save();

    $hash = $entity->get('entity_hash')->value;

    $entity->set('title', $this->randomString());
    $entity->save();

    $new_hash = $entity->get('entity_hash')->value;

    $this->assertNotEqual($hash, $new_hash);
  }

  /**
   * Test that hash change fires an event.
   */
  function testHashEvent() {
    $entity = $this->entityManager
      ->getStorage('node')
      ->create([
        'type' => 'test',
        'title' => $this
          ->randomString(),
      ]);
    $entity->save();

    $entity->set('title', $this->randomString());
    $entity->save();

    $event = $this->state->get('entity_hash_events_test.entity_hash_changed_event', []);
    $this->assertIdentical($event['event_name'], EntityHashChangedEvent::HASH_CHANGED);
    $this->assertIdentical($event['entity_id'], $entity->id());
  }

  /**
   * Reacts to entity hash change event.
   *
   * @param \Drupal\entity_hash\EntityHashChangedEvent $event
   *   Entity hash event.
   * @param string $name
   *   The event name.
   */
  function entityHashEventRecorder(EntityHashChangedEvent $event, $name) {
    $this->state->set('entity_hash_events_test.entity_hash_changed_event', [
      'event_name' => $name,
      'entity_id' => $event->getEntity()->id(),
    ]);
  }

}
