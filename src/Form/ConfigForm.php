<?php

namespace Drupal\entity_hash_watcher\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;

/**
 * Form definition for settings forms.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * Entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $entityTypeBundleInfo;

  /**
   * Entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Entity type manager service.
   *
   * @var Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The entity definition update manager.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  protected $entityDefinitionUpdateManager;

  /**
   * Form constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $entity_type_bundle_info
   *   Entity type bundle info.
   * @param \Drupal\Core\Entity\EntityFieldManager $entity_field_manager
   *   Entity field manager.
   * @param \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface $entity_definition_update_manager
   *   The entity definition update manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManager $entity_type_manager, EntityTypeBundleInfo $entity_type_bundle_info, EntityFieldManager $entity_field_manager, EntityDefinitionUpdateManagerInterface $entity_definition_update_manager) {
    $this->setConfigFactory($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityDefinitionUpdateManager = $entity_definition_update_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager'),
      $container->get('entity.definition_update_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_hash_watcher_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'entity_hash_watcher.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('entity_hash_watcher.config');
    $config_fields = $config->get('fields');
    $entity_types = $this->entityTypeManager->getDefinitions();
    $bundles = $this->entityTypeBundleInfo->getAllBundleInfo();
    $bundles_options = [];

    foreach ($entity_types as $entity_type => $definition) {
      $entity_type_key = (string) $definition->getLabel();
      $bundles_options[$entity_type_key] = [];
      $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type);

      foreach ($bundles as $bundle => $info) {
        $key = $entity_type . ':' . $bundle;
        $bundles_options[$entity_type_key][$key] = $info['label'];
      }

      asort($bundles_options[$entity_type_key]);
    }

    ksort($bundles_options);

    $form['#prefix'] = '<div id="wrapper">';
    $form['#suffix'] = '</div>';

    $form['bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Bundle'),
      '#options' => ['_none' => $this->t('- Select -')] + $bundles_options,
      '#ajax' => [
        'callback' => '::ajaxCallback',
        'wrapper' => 'wrapper',
      ],
    ];

    $bundle_key = $form_state->getValue('bundle');

    if ($bundle_key) {
      list($entity_type, $bundle) = explode(':', $bundle_key);
      $fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
      $fields_options = [];
      $fields_default = [];

      foreach ($fields as $field_id => $definition) {
        $fields_options[$field_id] = $definition->getLabel();
      }

      if (!empty($config_fields[$entity_type][$bundle])) {
        $fields_default = $config_fields[$entity_type][$bundle];
      }

      $form['fields'] = [
        '#type' => 'checkboxes',
        '#label' => $this->t('Fields'),
        '#options' => $fields_options,
        '#default_value' => $fields_default
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Ajax callback.
   */
  public function ajaxCallback(array &$form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $bundle = $form_state->getValue('bundle', '_none');

    if (empty($bundle) || $bundle == '_none') {
      $form_state->setErrorByName('bundle', $this->t('Please select value.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('entity_hash_watcher.config');
    $config_fields = $config->get('fields');

    $bundle_key = $form_state->getValue('bundle');
    $fields = $form_state->getValue('fields', []);
    list($entity_type, $bundle) = explode(':', $bundle_key);
    $fields = array_filter($fields);

    if (empty($fields) && !empty($config_fields[$entity_type][$bundle])) {
      unset($config_fields[$entity_type][$bundle]);

      if (empty($config_fields[$entity_type])) {
        unset($config_fields[$entity_type]);
      }
    }

    if (!empty($fields)) {
      $config_fields[$entity_type][$bundle] = array_values($fields);
    }

    $config->set('fields', $config_fields)->save();

    $this->entityDefinitionUpdateManager->applyUpdates();

    parent::submitForm($form, $form_state);
  }

}
